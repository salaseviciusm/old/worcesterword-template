$(document).ready(function() {

  // Mobile navigation toggle

  $('.toggle-nav').click(function(e) {
    $(this).toggleClass('active');
    $('.menu ul').toggleClass('active');

    e.preventDefault();
  });
});
