# HTML5/CSS3 Template
Screenshots:
* [Screenshot of index page](screenshot-index.png)
* [Screenshot of article page](screenshot-article.png)
* [Screenshot of contacts page](screenshot-contacts.png)

## Built With

HTML5/CSS3 <br>
jQuery

## Authors

* **Mantas Salasevicius** - *Developer* - [Portfolio](https://mantas.dev), [Gitlab](https://gitlab.com/mantas.dev)

## Date of development

2016

## License

This project is licensed under the GNU General Public License v3.0 - see the [LICENSE.md](LICENSE.md) file for details


